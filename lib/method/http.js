const http = require("http")
const https = require("https")
const StatusMethod = require("./../../config").StatusMethod;


module.exports = function (page) {
    return new Promise ((resolve, reject) => {
        var httpProtocol = page.method == StatusMethod.HTTP ? http : https;

        function fail(reason, err, req) {
            resolve({
                success: false,
                reason: reason,
                err: err,
                page: page
            });
            req.abort();
        }

        var options = page.options;
        var req = httpProtocol.request(options, res => {
            res.on("error", err => {
                fail("error", err, req);
            })
            if(res.statusCode != page.shouldBe){
                fail("wrong response code [" + res.statusCode + "]", null, req);
            }else{
                resolve({
                    success: true,
                    page: page
                })
            }
        })
        req.on("error", err => {
            fail("error", err, req)
        })
        req.on("timeout", () => {
            fail("timeout", null, req)
        })
        req.end();
    })
}
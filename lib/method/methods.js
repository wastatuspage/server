const StatusMethod = require("./../../config").StatusMethod

var methods = {}

for(let method of Object.keys(StatusMethod)){
    methods[StatusMethod[method]] = require("./" + method.toLowerCase())
}

module.exports = methods
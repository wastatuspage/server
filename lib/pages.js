
const config = require("./../config").options;
const IncidentEvent = require("./../config").IncidentEvent;

const fetchStatus = require("./fetchStatus")()

module.exports = function (pagesConfig) {

    this.pages = initialisePages(pagesConfig);

    this.getPage = (id) => {
        for(let page of this.pages){
            if(page.id == id){
                return page; 
            }
        } 
    }

    this.scheduleFetchForAll = () => {
        for(let page of this.pages){
            this.scheduleFetch(page)
        }
    }

    this.scheduleFetch = (page) => {
        fetchStatus.fetch(page).then(result => {
            page.status.online = result.success;

            if(!result.success){
                if(page.status.incidents.length > 0 && !page.status.incidents[page.status.incidents.length - 1].end){
                    page.status.incidents[page.status.incidents.length - 1].reason = result.reason
                }else{
                    console.log("New downtime on " + result.page.name + " detected")
                    page.status.incidents.push({
                        start: new Date().getTime(),
                        event: IncidentEvent.DOWNTIME,
                        reason: {
                            type: result.reason,
                            error: result.err
                        }
                    })
                }
            }else{
                if(page.status.incidents.length > 0 && !page.status.incidents[page.status.incidents.length - 1].end){
                    page.status.incidents[page.status.incidents.length - 1].end = new Date().getTime();
                }
            }
            
            setTimeout((self) => {
                self.scheduleFetch(page)
            }, page.interval, this)
        })
    }


    return this;
}

function initialisePages (pagesConfig) {
    var pages = [];

    for(let i = 0; i < pagesConfig.length; i++){
        var p = pagesConfig[i];
        p.id = i;
        p.status = {
            online: null,
            onlineSince: new Date().getTime(),
            incidents: []
        }
        pages.push(p)
    }

    return pages;
}
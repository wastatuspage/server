const StatusMethod = require("./../config").StatusMethod;
const methods = require("./method/methods")

module.exports = function () {
    return {

        fetch: function (page) {
            return methods[page.method](page)
        }

    }
}
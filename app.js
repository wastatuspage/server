
const fs = require("fs")
const app = require("express")();

const config = require("./config").options;
const IncidentEvent = require("./config").IncidentEvent;

const pages = require("./lib/pages")(config.pages);
const fetchStaus = require("./lib/fetchStatus")(config.pages)


app.use(require("cors")())

app.get("/status", (req, res) => {
    res.send(JSON.stringify(pages.pages));
});

pages.scheduleFetchForAll();


console.log("Server started")
app.listen(config.port)
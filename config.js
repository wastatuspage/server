module.exports.StatusMethod = {
    HTTP: 0,
    HTTPS: 1,
    PING: 2,
}

module.exports.IncidentEvent = {
    DOWNTIME: 0
}

module.exports.options = {
    
    port: 8800,

    pages: [
        {
            name: "Failure",
            method: this.StatusMethod.HTTP,
            options: {
                host: "localhost",
                path: "",
                port: 8084,
                timeout: 2000
            },
            interval: 1000*60,
            shouldBe: 200
        },
        {
            name: "Website",
            method: this.StatusMethod.HTTPS,
            options: {
                host: "walamana.de",
                path: "",
            },
            interval: 1000*60,
            shouldBe: 200
        },
        {
            name: "Miniplan Client",
            method: this.StatusMethod.HTTPS,
            options: {
                host: "minis.walamana.de",
                path: "",
            },
            interval: 1000*60,
            shouldBe: 200
        },
        {
            name: "Miniplan API",
            method: this.StatusMethod.HTTPS,
            options: {
                host: "minis.walamana.de",
                path: "/api/",
            },
            interval: 1000*60,
            shouldBe: 200
        },
        {
            name: "CardsAgainstHumanity Client",
            method: this.StatusMethod.HTTPS,
            options: {
                host: "cardsagainst.walamana.de",
                path: "",
            },
            interval: 1000*60,
            shouldBe: 200
        },
        {
            name: "CardsAgainstHumanity Server",
            method: this.StatusMethod.HTTPS,
            options: {
                host: "cardsagainst.walamana.de",
                path: "/api/",
            },
            interval: 1000*60,
            shouldBe: 200
        }
    ]
}